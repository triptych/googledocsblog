package googleDocsBlog

import org.thymeleaf.TemplateEngine
import org.thymeleaf.context.Context
import org.thymeleaf.templatemode.TemplateMode
import org.thymeleaf.templateresolver.StringTemplateResolver
import java.io.File

data class HtmlTemplate(val html: String, val context: Context)

fun generateHtmlTemplate(documentName: String, document: DocumentHtml, documentNames: List<String>?): HtmlTemplate {
    val context = Context()
    val html = File("template.html").readText(Charsets.UTF_8)

    context.setVariable("documentName", "Ben Wiser | ${documentName}")
    context.setVariable("documentNames", documentNames)

    context.setVariable("style", document.style)
    context.setVariable("content", document.body)

    return HtmlTemplate(html, context)
}

fun generateIndexHtmlTemplate(documentNames: List<String>?): HtmlTemplate {
    val context = Context()
    val html = File("template.html").readText(Charsets.UTF_8)
    context.setVariable("documentName", "Ben Wiser | Blog")
    context.setVariable("documentNames", documentNames)
    context.setVariable("content", "")
    return HtmlTemplate(html, context)
}

fun renderHtml(htmlTemplate: HtmlTemplate): String {
    val templateEngine = TemplateEngine()
    val templateResolver = StringTemplateResolver()
    templateResolver.templateMode = TemplateMode.HTML
    templateEngine.setTemplateResolver(templateResolver)
    return templateEngine.process(htmlTemplate.html, htmlTemplate.context)
}

fun writeFile(path: String, html: String) = File(path).writeText(html, Charsets.UTF_8)